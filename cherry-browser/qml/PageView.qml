import QtQuick 1.1
import QtWebKit 1.1
import QtDesktop 0.1
import "../js/util.js" as Util

Rectangle {
    id: pageView

    property string url: "about:blank"
    property string title: webView.title

    function goUrl(url) {
        var urlAutoCorrectRules = []
        urlAutoCorrectRules.push(function(url) {
            if(Util.urlProtocol(url) === '')
                return 'http://' + url
            else
                return url
        })

        url = Util.urlAutoCorrect(url, urlAutoCorrectRules)

        webView.url = url
    }

    function selectUrl() {
        urlField.focus = true
        urlField.selectAll()
    }

    onUrlChanged: if(webView.url != url) goUrl(url)

    gradient: Gradient {
        GradientStop { position: 0; color: Qt.tint(pageView.color, Qt.rgba(.7, .7, .7, .8)) }
        GradientStop { position: 1; color: Qt.tint(pageView.color, Qt.rgba(.5, .5, .5, .8)) }
    }

    Rectangle {
        id: topBar

        property int buttonSize: urlField.implicitHeight
        property int buttonIconSize: 16

        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right

        gradient: Gradient {
            GradientStop { position: 0; color: pageView.color }
            GradientStop { position: 1; color: Qt.tint(pageView.color, Qt.rgba(0, 0, 0, .333)) }
        }

        height: urlField.implicitHeight + 6

        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: 1
            anchors.rightMargin: 1
            spacing: 2

            ButtonRow {
                ToolButton {
                    id: backButton
                    width: topBar.buttonSize
                    height: topBar.buttonSize
                    iconName: "back"
                    iconSize: topBar.buttonIconSize
                    enabled: webView.back.enabled
                    onClicked: webView.back.trigger()
                }

                ToolButton {
                    id: forwardButton
                    width: topBar.buttonSize
                    height: topBar.buttonSize
                    iconName: "forward"
                    iconSize: topBar.buttonIconSize
                    enabled: webView.forward.enabled
                    onClicked: webView.forward.trigger()
                }
            }

            TextField {
                id: urlField

                topMargin: 0
                bottomMargin: 0
                leftMargin: 4
                rightMargin: 4
                implicitHeight: 25

                Layout.horizontalSizePolicy: Layout.Expanding

                Keys.onReturnPressed: {
                    goUrl(text)
                }

                Keys.onEscapePressed: {
                    console.debug("esc")
                    urlField.focus = false
                    webView.focus = true
                }
            }

            ButtonRow {
                ToolButton {
                    id: stopButton
                    width: topBar.buttonSize
                    height: topBar.buttonSize
                    iconName: "stop"
                    iconSize: topBar.buttonIconSize
                    enabled: webView.stop.enabled
                    onClicked: webView.stop.trigger()
                }

                ToolButton {
                    id: reloadButton
                    width: topBar.buttonSize
                    height: topBar.buttonSize
                    iconName: "reload"
                    iconSize: topBar.buttonIconSize
                    enabled: webView.reload.enabled
                    onClicked: webView.reload.trigger()
                }

                /*
                ToolButton {
                    id: combinedReloadStopButton
                    property variant action: webView.stop.enabled ? webView.stop : webView.reload
                    width: topBar.buttonSize
                    height: topBar.buttonSize
                    iconName: webView.stop.enabled ? "stop" : "reload"
                    iconSize: topBar.buttonIconSize
                    enabled: action.enabled
                    onClicked: action.trigger()
                }
                */
            }
        }

        // Progress bar
        Rectangle {
            visible: webView.progress != 1
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            height: 5

            gradient: Gradient {
                GradientStop { position:  0; color: "#222" }
                GradientStop { position: .8; color: "#444" }
                GradientStop { position:  1; color: "#888" }
            }

            Rectangle {
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                anchors.bottomMargin: 1
                width: parent.width * webView.progress
                border { color: Qt.rgba(1, 1, 1, .5); width: 1 }

                gradient: Gradient {
                    GradientStop { position:  0; color: "#fff" }
                    GradientStop { position: .5; color: "#8f8" }
                    GradientStop { position:  1; color: "#3a3" }
                }
            }
        }
    }

    ScrollArea {
        id: scrollArea

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: topBar.bottom
        anchors.bottom: parent.bottom

        frame: false

        WebView {
            id: webView

            pressGrabTime: 0
            smooth: false
            visible: url != "about:blank"

            onUrlChanged: { console.log("navigate: " + url); urlField.text = url }

            onWidthChanged: scrollArea.updateContentSize(width, height)
            onHeightChanged: scrollArea.updateContentSize(width, height)
        }

        function updateSize() {
            // check if the page will fit in width
            webView.preferredWidth = scrollArea.width
            scrollArea.horizontalScrollBar.visible = (webView.width > scrollArea.width)

            webView.preferredHeight = function(){ return scrollArea.viewportHeight; }
            webView.preferredWidth = function(){ return scrollArea.viewportWidth; }
        }

        function updateContentSize(w, h) {
            contentWidth = Math.max(width, w);
            contentHeight = Math.max(height, h);
        }

        Timer {
            running: true
            interval: 100
            repeat: true
            onTriggered: scrollArea.updateSize()
        }
    }

    Component.onCompleted: {
        scrollArea.verticalScrollBar.singleStep = 100
        scrollArea.horizontalScrollBar.singleStep = 100
        goUrl(url)
    }
}
