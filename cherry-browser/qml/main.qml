import QtQuick 1.1
import "../js/util.js" as Util

Item {
    id: window

    property int currentPageIndex: pagesModel.currentIndex
    property string currentPageTitle: pagesModel.currentPageTitle

    signal titleChanged(string x);
    signal close();

    onCurrentPageTitleChanged: {
        console.log(currentPageTitle)
        titleChanged(currentPageTitle)
    }

    focus: true

    Rectangle {
        id: tabsViewport
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top

        height: 30
        clip: true

        gradient: Gradient {
            GradientStop { position: 0; color: Qt.rgba(.2, .2, .2, 1) }
            GradientStop { position: 1; color: Qt.rgba(.6, .6, .6, 1) }
        }

        Row {
            id: tabsViewWrapper

            anchors.fill: parent
            anchors.topMargin: 4
            anchors.leftMargin: 4
            anchors.rightMargin: 4

            spacing: 6

            Row {
                id: tabsView

                anchors.top: parent.top
                anchors.bottom: parent.bottom

                spacing: 4

                add: Transition { NumberAnimation { properties: "width"; duration: 250; easing.type: Easing.OutQuad } }

                move: Transition { NumberAnimation { properties: "x"; duration: 250; easing.type: Easing.OutQuad } }

                Repeater {
                    model: pagesModel

                    Rectangle {
                        id: tabDelegate

                        property real hover: ma.containsMouse
                        Behavior on hover { SmoothedAnimation { velocity: 10 } }

                        onHoverChanged: {}

                        property real current: index == pagesModel.currentIndex
                        Behavior on current { SmoothedAnimation { velocity: 10 } }

                        property real hoverOrCurrent: Math.max(current, hover)

                        property color baseColor: pageColor

                        width: 200
                        height: parent.height + radius // in combination with clipping, this results in hiding the bottom rounded corners
                        radius: 7
                        y: (1 - hoverOrCurrent) * 2

                        gradient: Gradient {
                            id: gradient
                            property color baseColorModified: Qt.tint(tabDelegate.baseColor, Qt.rgba(0, 0, 0, .3 * (1 - tabDelegate.hoverOrCurrent)))
                            GradientStop { position:  0; color: Qt.lighter(gradient.baseColorModified, 1.2) }
                            GradientStop { position: .2; color: gradient.baseColorModified }
                            GradientStop { position:  1; color: Util.color.mix(Qt.darker(gradient.baseColorModified, 1.8), gradient.baseColorModified, current) }
                        }

                        border { width: 2; color: Qt.rgba(0, 0, 0, .3) }

                        Text {
                            anchors.fill: parent
                            anchors.bottomMargin: parent.radius // we have hidden the bottom rounded corners of the parent item
                            anchors.leftMargin: 5
                            anchors.rightMargin: 0
                            anchors.topMargin: 2

                            verticalAlignment: Qt.AlignVCenter
                            horizontalAlignment: Qt.AlignLeft

                            clip: true
                            color: "black"

                            text: pageTitle.length === 0 ? "Neuer Tab" : pageTitle
                        }

                        MouseArea {
                            id: ma
                            anchors.fill: parent
                            hoverEnabled: true
                            acceptedButtons: Qt.LeftButton | Qt.MiddleButton

                            onClicked: {
                                switch(mouse.button) {
                                    case Qt.LeftButton: pagesModel.currentIndex = index; break;
                                    case Qt.MiddleButton: pagesModel.closePage(index); break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        id: pagesViewport

        anchors.top: tabsViewport.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        gradient: Gradient {
            GradientStop { position: 0; color: Qt.rgba(.7, .7, .7, 1) }
            GradientStop { position: 1; color: Qt.rgba(.5, .5, .5, 1) }
        }

        Item {
            id: pagesViewWrapper
            anchors.fill: parent

            Row {
                id: pagesView

                x: -pagesModel.currentIndex * parent.width
                Behavior on x { SmoothedAnimation { duration: 150 } }

                anchors.top: parent.top
                anchors.bottom: parent.bottom

                function get(i) {
                    return children[i]
                }

                Repeater {
                    model: pagesModel

                    PageView {
                        id: pageView

                        width: pagesViewWrapper.width
                        height: parent ? parent.height : 0
                        url: url
                        color: pageColor

                        onTitleChanged: pagesModel.setProperty(index, 'pageTitle', pageView.title)
                    }
                }
            }
        }
    }

    ListModel {
        id: pagesModel

        property int currentIndex: 0
        property string currentPageTitle: currentIndex < count ? get(currentIndex).pageTitle : ""


        function newPage(url) {
            append(__createPage(url))
            var newIndex = count - 1
            currentIndex = newIndex  // make the new page current
            pagesView.children[newIndex].goUrl(url)
        }

        function closePage(index) {
            if(currentIndex > index || (currentIndex === index && index === count - 1 && index !== 0))
                currentIndex--  // also move the current index by 1 if current index was after the closed page
            remove(index)
            if(count == 0)
                window.close()
        }

        function __createPage(url) {
            return {
                url: url,
                pageTitle: "New tab",
                pageColor: "#e0e0e0"
            }
        }
    }

    Keys.onPressed: {
        if(event.modifiers & Qt.ControlModifier) {
            if(event.key === Qt.Key_L) {
                var i = pagesModel.currentIndex
                var tabView = pagesView.children[i]
                tabView.selectUrl()
            } else if(event.key === Qt.Key_T) {
                pagesModel.newPage('about:blank')
                var newIndex = pagesModel.currentIndex
                pagesView.get(newIndex).selectUrl()
            } else if(event.key === Qt.Key_W) {
                pagesModel.closePage(pagesModel.currentIndex)
            } else if(event.key === Qt.Key_Q) {
                window.close()
            } else if(event.key === Qt.Key_Tab || event.key === Qt.Key_Backtab) {
                var tabDelta = event.key === Qt.Key_Tab ? 1 : -1
                console.log('tab delta ' + tabDelta)
                pagesModel.currentIndex = (pagesModel.currentIndex + tabDelta + pagesModel.count) % pagesModel.count
            }
        }
    }

    Component.onCompleted: {
        pagesModel.newPage('about:blank')
        pagesView.get(0).selectUrl()
    }

    onCurrentPageIndexChanged: {
        window.focus = true
    }
}
