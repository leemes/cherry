#ifndef MAIN_H
#define MAIN_H

#include <QtGlobal>

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QApplication>
typedef QApplication QGuiApplication;
#else
#include <QGuiApplication>
#endif



#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
class QDeclarativeView;
typedef QDeclarativeView QQuickView;
#else
class QQuickView;
#endif

class Application : public QGuiApplication
{
    Q_OBJECT
    
public:
    Application(int & argc, char **argv);
    ~Application();

public slots:
    void qmlTitleChanged(QString);

private:
    QQuickView *qmlView;
};

#endif // MAIN_H
