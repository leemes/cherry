.pragma library

var math = {
    mix: function(value1, value2, mix) {
        return value1 * (1 - mix) + value2 * mix
    }
}

var color = {
    components: function(c) {
        var cstr = c + ''
        return [
            parseInt(cstr.substring(1, 3), 16) / 255.0,
            parseInt(cstr.substring(3, 5), 16) / 255.0,
            parseInt(cstr.substring(5, 7), 16) / 255.0,
        ]
    },
    red: function(c) {
        return color.components(c)[0]
    },
    green: function(c) {
        return color.components(c)[1]
    },
    blue: function(c) {
        return color.components(c)[2]
    },

    mix: function(color1, color2, mix) {
        var c1 = color.components(color1)
        var c2 = color.components(color2)
        var r = math.mix(c1[0], c2[0], mix)
        var g = math.mix(c1[1], c2[1], mix)
        var b = math.mix(c1[2], c2[2], mix)
        return Qt.rgba(r, g, b, 1)
    }
}

function urlAutoCorrect(url, rules) {
    rules.forEach(function(rule) { url = rule(url); })
    return url
}

function urlProtocol(url) {
    var protocolSep = url.indexOf(':')
    return url.substr(0, protocolSep)
}

