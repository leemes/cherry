#include "main.h"
#include <QUrl>
#include <QWebSettings>

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QDeclarativeView>
#include <QDeclarativeEngine>
#include <QGraphicsObject>
typedef QDeclarativeEngine QQuickEngine;
typedef QGraphicsObject QQuickItem;
#else
#include <QQuickView>
#include <QQuickEngine>
#include <QQuickItem>
#endif

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QGLWidget>
#endif




int main(int argc, char *argv[])
{
    Application app(argc, argv);
    return app.exec();
}



Application::Application(int &argc, char **argv)
    : QGuiApplication(argc, argv)
{
    qmlView = new QQuickView();
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
//    qmlView->setViewport(new QGLWidget);
#endif
    qmlView->setSource(QUrl("qml/main.qml"));
    //QQuickEngine *engine = qmlView->engine();

    QQuickItem *qmlRootItem = qmlView->rootObject();

    // Update the window title whenever the current page's title changes
    connect(qmlRootItem, SIGNAL(titleChanged(QString)), SLOT(qmlTitleChanged(QString)));
    qmlTitleChanged(""); // initial call

    // Close the window when QML wants to
    connect(qmlRootItem, SIGNAL(close()), qmlView, SLOT(close()));

    qmlView->resize(900, 600);
    qmlView->setResizeMode(QQuickView::SizeRootObjectToView);
    qmlView->show();

    QWebSettings::globalSettings()->setAttribute(QWebSettings::PluginsEnabled, true);
}

void Application::qmlTitleChanged(QString pageTitle)
{
    QString windowTitleSeparator = " - ";
    QString windowTitleSuffix = "Cherry";
    qmlView->setWindowTitle(pageTitle.isEmpty() ? windowTitleSuffix : pageTitle + windowTitleSeparator + windowTitleSuffix);
}

Application::~Application()
{
    delete qmlView;
}

